#include <SFE_BMP180.h>
#include <Wire.h>
#include <Ethernet.h>
#include <DHT.h>

#define MACADDRESS 0xDE, 0xDE, 0xBE, 0xEF, 0xFE, 0xEE 
#define IP 192,168,0,167
#define LISTENPORT 10050
String hostname = "arduino";


#define DHTPIN 6
#define DHTYPE 22
#define DHTTYPE DHT22
String BMPTYPE = "BMP180";

#define ZABBIXPING 1 //The default value for zabbix's agent protocol
#define ZABBIXPROTOCOL 3.0 //zabbix agent protocol version
#define CODEVERSION 2.0

#define ITEMS_SIZE  12
String items[] = {"agent.ping", "agent.hostname", "agent.version", "dht.humidity", "bmp180", "dht.type", "bmp.type", "pressure", "voltage", "heat.indexC", "product.version", "temperature"};

// You will need to create an SFE_BMP180 object, here called "pressure":

SFE_BMP180 pressure;

#define ALTITUDE 1655.0 // Altitude of SparkFun's HQ in Boulder, CO. in meters

EthernetServer server = EthernetServer(LISTENPORT);

DHT dht(DHTPIN,DHTYPE);
DHT sensor(DHTPIN, DHTTYPE);
String msg ="";

void setup()
{
  uint8_t mac[6] = {MACADDRESS};
  uint8_t ip[4] = {IP};

  Ethernet.begin(mac,ip);
  server.begin();
  sensor.begin( );
  //DHT Sensor
  dht.begin();
  
  Serial.begin(9600);
  Serial.println("REBOOT");

  // Initialize the sensor (it is important to get calibration values stored on the device).

  if (pressure.begin())
    Serial.println("BMP180 init success");
  else
  {
    // Oops, something went wrong, this is usually a connection problem,
    // see the comments at the top of this sketch for the proper connections.

    Serial.println("BMP180 init fail\n\n");
   
  }
}

void loop()
{
  float humidity = sensor.readHumidity( );
  float temperature_C = sensor.readTemperature( );
  float temperature_F = sensor.readTemperature (true);
  
  if (isnan(humidity) || isnan(temperature_C) || isnan(temperature_F)) 
  {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
  float heat_indexF = sensor.computeHeatIndex(temperature_F, humidity);
  float heat_indexC = sensor.convertFtoC(heat_indexF);
  //Serial.println(temperature_C);
  //Serial.println(temperature_F);
  Serial.println(heat_indexC);
  
  char status;
  double T,P,p0,a;

  // Loop here getting pressure readings every 10 seconds.

  // If you want sea-level-compensated pressure, as used in weather reports,
  // you will need to know the altitude at which your measurements are taken.
  // We're using a constant called ALTITUDE in this sketch:
  
  Serial.println();
  Serial.print("provided altitude: ");
  Serial.print(ALTITUDE,0);
  Serial.print(" meters, ");
  Serial.print(ALTITUDE*3.28084,0);
  Serial.println(" feet");
  
  // If you want to measure altitude, and not pressure, you will instead need
  // to provide a known baseline pressure. This is shown at the end of the sketch.

  // You must first get a temperature measurement to perform a pressure reading.
  
  // Start a temperature measurement:
  // If request is successful, the number of ms to wait is returned.
  // If request is unsuccessful, 0 is returned.

  status = pressure.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:
    delay(status);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Function returns 1 if successful, 0 if failure.

    status = pressure.getTemperature(T);
    if (status != 0)
    {
      // Print out the measurement:
      Serial.print("temperature: ");
      Serial.print(T,2);
      Serial.print(" deg C, ");
      
      // Start a pressure measurement:
      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      // If request is successful, the number of ms to wait is returned.
      // If request is unsuccessful, 0 is returned.

      status = pressure.startPressure(3);
      if (status != 0)
      {
        // Wait for the measurement to complete:
        delay(status);

        // Retrieve the completed pressure measurement:
        // Note that the measurement is stored in the variable P.
        // Note also that the function requires the previous temperature measurement (T).
        // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
        // Function returns 1 if successful, 0 if failure.

        status = pressure.getPressure(P,T);
        if (status != 0)
        {
          // Print out the measurement:
          Serial.print("absolute pressure: ");
          Serial.print(P,2);
          Serial.print(" mb, ");
        
        }
        else Serial.println("error retrieving pressure measurement\n");
      }
      else Serial.println("error starting pressure measurement\n");
    }
    else Serial.println("error retrieving temperature measurement\n");
  }
  else Serial.println("error starting temperature measurement\n");
  
  int sensorValue = analogRead(A0);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue * (5.0 / 1023.0);
  // print out the value you read:
  //Serial.println(voltage);

  
  EthernetClient client = server.available();
  if (client) {
    if (client.available() > 0) {
      char thisChar = client.read();
      if (thisChar == '\n') {

        switch(findId(msg)){
          case 0:
            server.println(ZABBIXPING);
            break;
          case 1:
            server.println(hostname);
            break;
          case 2:
            server.println(ZABBIXPROTOCOL);
            break;  
          case 3:
            server.println(humidity);
            break;
          case 4:
            server.println(T,2);
            break;
          case 5:
            server.println(DHTYPE);
            break;
          case 6:
            server.println(BMPTYPE);
            break;
          case 7:
            server.println(P,2);
            break;
          case 8:
             server.println(voltage);
             break;
          case 9:
             server.println(heat_indexC);
             Serial.println(heat_indexC);
             break;
          case 10:
             server.println(CODEVERSION);
             break;
          case 11:
             server.println(temperature_C);
             break; 
          default:
            server.println("ZBX_NOTSUPPORTED");
        }

        client.stop();
        msg="";
      }else {
        msg += thisChar;  
      }
    }
  }
}


int findId(String text) {
  int returnValue=-1;
  for (int i=0; i < ITEMS_SIZE; i++){
    if(items[i].equals(text)){
      returnValue = i; 
    }
  }
  return returnValue;
}